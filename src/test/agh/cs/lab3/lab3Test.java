package agh.cs.lab3;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;



public class lab3Test {

    private Animal animal;
    private String[] directions1, directions2, directions3, directions4;
    private MoveDirection[] parsedDirections1, parsedDirections2, parsedDirections3, parsedDirections4;

    @Before
    public void init() {
        animal = new Animal();

        directions1 = new String[]{"f", "l", "b", "r", "l"};
        parsedDirections1 = new MoveDirection[]{MoveDirection.FORWARD, MoveDirection.LEFT,
                MoveDirection.BACKWARD, MoveDirection.RIGHT, MoveDirection.LEFT};

        directions2 = new String[]{"left", "backward", "forward", "right", "x", "right", "backward", "backward",
                "backward", "backward", "backward"};
        parsedDirections2 = new MoveDirection[]{MoveDirection.LEFT, MoveDirection.BACKWARD,
                MoveDirection.FORWARD, MoveDirection.RIGHT, MoveDirection.RIGHT, MoveDirection.BACKWARD,
                MoveDirection.BACKWARD, MoveDirection.BACKWARD, MoveDirection.BACKWARD, MoveDirection.BACKWARD};

        directions3 = new String[]{"left", "f", "b", "asd", "r", "forward", "l", "forward", "f", "f", "f", "l", "b"};
        parsedDirections3 = new MoveDirection[]{MoveDirection.LEFT, MoveDirection.FORWARD,
                MoveDirection.BACKWARD, MoveDirection.RIGHT, MoveDirection.FORWARD, MoveDirection.LEFT,
                MoveDirection.FORWARD, MoveDirection.FORWARD, MoveDirection.FORWARD, MoveDirection.FORWARD,
                MoveDirection.LEFT, MoveDirection.BACKWARD};

        directions4 = new String[]{"xd", "r", "f", "l", "f", "forward", "left", "forward", "f", "b", "right", "a"};
        parsedDirections4 = new MoveDirection[]{MoveDirection.RIGHT, MoveDirection.FORWARD,
                MoveDirection.LEFT, MoveDirection.FORWARD, MoveDirection.FORWARD, MoveDirection.LEFT,
                MoveDirection.FORWARD, MoveDirection.FORWARD, MoveDirection.BACKWARD, MoveDirection.RIGHT};
    }

    @Test
    public void parse1Test() {
        MoveDirection[] testedDirections1 = OptionsParser.parse(directions1);

        Assert.assertArrayEquals(testedDirections1, parsedDirections1);
    }

    @Test
    public void parse2Test() {
        MoveDirection[] testedDirections2 = OptionsParser.parse(directions2);

        Assert.assertArrayEquals(testedDirections2, parsedDirections2);
    }

    @Test
    public void parse3Test() {
        MoveDirection[] testedDirections3 = OptionsParser.parse(directions3);

        Assert.assertArrayEquals(testedDirections3, parsedDirections3);
    }

    @Test
    public void parse4Test() {
        MoveDirection[] testedDirections4 = OptionsParser.parse(directions4);

        Assert.assertArrayEquals(testedDirections4, parsedDirections4);
    }

    @Test
    public void animal1Test() {
        MoveDirection[] parsed_directions = OptionsParser.parse(directions1);
        for (MoveDirection direction : parsed_directions)
            animal.move(direction);

        Assert.assertEquals(animal.getOrientation(), MapDirection.WEST);
        Assert.assertEquals(animal.getPosition(), new Vector2d(3, 3));
    }

    @Test
    public void animal2Test() {
        MoveDirection[] parsed_directions = OptionsParser.parse(directions2);
        for (MoveDirection direction : parsed_directions)
            animal.move(direction);

        Assert.assertEquals(animal.getOrientation(), MapDirection.EAST);
        Assert.assertEquals(animal.getPosition(), new Vector2d(0, 2));
    }

    @Test
    public void animal3Test() {
        MoveDirection[] parsed_directions = OptionsParser.parse(directions3);
        for (MoveDirection direction : parsed_directions)
            animal.move(direction);

        Assert.assertEquals(animal.getOrientation(), MapDirection.SOUTH);
        Assert.assertEquals(animal.getPosition(), new Vector2d(0, 4));
    }

    @Test
    public void animal4Test() {
        MoveDirection[] parsed_directions = OptionsParser.parse(directions4);
        for (MoveDirection direction : parsed_directions)
            animal.move(direction);

        Assert.assertEquals(animal.getOrientation(), MapDirection.NORTH);
        Assert.assertEquals(animal.getPosition(), new Vector2d(2, 4));
    }
}
