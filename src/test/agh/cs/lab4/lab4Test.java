package agh.cs.lab4;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class lab4Test {

    private IWorldMap map;
    private Animal animalA, animalB;

    @Before
    public void init() {
        map = new RectangularMap(10, 5);
        animalA = new Animal(map);
        map.place(animalA);
        animalB = new Animal(map, new Vector2d(3,4));
        map.place(animalB);
    }

    @Test
    public void animal1Test() {
        String[] directions1 = new String[]{"f", "b", "r", "l", "f", "f"};
        MoveDirection[] parsedDirections1 = OptionsParser.parse(directions1);
        map.run(parsedDirections1);
        Assert.assertEquals(animalA.getPosition(), new Vector2d(2, 3));
        Assert.assertEquals(animalA.getOrientation(), MapDirection.EAST);

        Assert.assertEquals(animalB.getPosition(), new Vector2d(3, 3));
        Assert.assertEquals(animalB.getOrientation(), MapDirection.WEST);
    }

    @Test
    public void animal2Test() {
        String[] directions2 = new String[]{"f", "b", "r", "l", "f", "f", "r", "r", "f", "f"};
        MoveDirection[] parsedDirections2 = OptionsParser.parse(directions2);
        map.run(parsedDirections2);
        Assert.assertEquals(animalA.getPosition(), new Vector2d(2, 2));
        Assert.assertEquals(animalA.getOrientation(), MapDirection.SOUTH);

        Assert.assertEquals(animalB.getPosition(), new Vector2d(3, 4));
        Assert.assertEquals(animalB.getOrientation(), MapDirection.NORTH);
    }

    @Test
    public void animal3Test() {
        String[] directions3 = new String[]{"f", "b", "r", "l", "f", "f", "r", "r", "f", "f", "f", "f", "f", "f", "f", "f"};
        MoveDirection[] parsedDirections3 = OptionsParser.parse(directions3);
        map.run(parsedDirections3);
        Assert.assertEquals(animalA.getPosition(), new Vector2d(2, 0));
        Assert.assertEquals(animalA.getOrientation(), MapDirection.SOUTH);

        Assert.assertEquals(animalB.getPosition(), new Vector2d(3, 5));
        Assert.assertEquals(animalB.getOrientation(), MapDirection.NORTH);
    }
}
