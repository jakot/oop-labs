package agh.cs.lab7;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class RectangularMapTest {

    private IWorldMap map;
    private Animal animalA, animalB;

    @Before
    public void init() {
        map = new RectangularMap(10, 5);
        animalA = new Animal(map);
        map.place(animalA);
        animalB = new Animal(map, new Vector2d(3,4));
        map.place(animalB);
    }

    @Test
    public void map1Test() {
        String[] directions1 = new String[]{"f", "b", "r", "l", "f", "f"};
        MoveDirection[] parsedDirections1 = OptionsParser.parse(directions1);
        map.run(parsedDirections1);
        Assert.assertEquals(animalA.getPosition(), new Vector2d(2, 3));
        Assert.assertEquals(animalA.getOrientation(), MapDirection.EAST);

        Assert.assertEquals(animalB.getPosition(), new Vector2d(3, 3));
        Assert.assertEquals(animalB.getOrientation(), MapDirection.WEST);
    }

    @Test
    public void map2Test() {
        String[] directions2 = new String[]{"f", "b", "r", "l", "f", "f", "r", "r", "f", "f"};
        MoveDirection[] parsedDirections2 = OptionsParser.parse(directions2);
        map.run(parsedDirections2);
        Assert.assertEquals(animalA.getPosition(), new Vector2d(2, 2));
        Assert.assertEquals(animalA.getOrientation(), MapDirection.SOUTH);

        Assert.assertEquals(animalB.getPosition(), new Vector2d(3, 4));
        Assert.assertEquals(animalB.getOrientation(), MapDirection.NORTH);
    }

    @Test
    public void map3Test() {
        String[] directions3 = new String[]{"f", "b", "r", "l", "f", "f", "r", "r", "f", "f", "f", "f", "f", "f", "f", "f"};
        MoveDirection[] parsedDirections3 = OptionsParser.parse(directions3);
        map.run(parsedDirections3);
        Assert.assertEquals(animalA.getPosition(), new Vector2d(2, 0));
        Assert.assertEquals(animalA.getOrientation(), MapDirection.SOUTH);

        Assert.assertEquals(animalB.getPosition(), new Vector2d(3, 5));
        Assert.assertEquals(animalB.getOrientation(), MapDirection.NORTH);
    }

    @Test(expected = IllegalArgumentException.class)
    public void map4Test() {
        map.place(new Animal(map, animalA.getPosition()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void map5Test() {
        String[] directions4 = new String[]{"f", "b", "r", "l", "f", "i", "r", "r", "f", "f", "f", "f", "f", "f", "f"};
        MoveDirection[] parsedDirections4 = OptionsParser.parse(directions4);
        map.run(parsedDirections4);
    }
}
