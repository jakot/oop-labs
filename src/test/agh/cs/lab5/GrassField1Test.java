package agh.cs.lab5;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class GrassField1Test {

    private IWorldMap map;
    private Animal animalA, animalB;

    @Before
    public void init() {
        ArrayList<Grass> grasses = new ArrayList<>();
        grasses.add(new Grass(new Vector2d(-4, -4)));
        grasses.add(new Grass(new Vector2d(7, 7)));
        grasses.add(new Grass(new Vector2d(3, 6)));
        grasses.add(new Grass(new Vector2d(2, 0)));
        map = new GrassField(grasses);
        animalA = new Animal(map);
        map.place(animalA);
        animalB = new Animal(map, new Vector2d(3,4));
        map.place(animalB);
    }

    @Test
    public void map1Test() {
        String[] directions1 = new String[]{"f", "b", "r", "l", "f", "f"};
        MoveDirection[] parsedDirections1 = OptionsParser.parse(directions1);
        map.run(parsedDirections1);
        Assert.assertEquals(animalA.getPosition(), new Vector2d(2, 3));
        Assert.assertEquals(animalA.getOrientation(), MapDirection.EAST);

        Assert.assertEquals(animalB.getPosition(), new Vector2d(3, 3));
        Assert.assertEquals(animalB.getOrientation(), MapDirection.WEST);
    }

    @Test
    public void map2Test() {
        String[] directions2 = new String[]{"f", "b", "r", "l", "f", "f", "r", "r", "f", "f"};
        MoveDirection[] parsedDirections2 = OptionsParser.parse(directions2);
        map.run(parsedDirections2);
        Assert.assertEquals(animalA.getPosition(), new Vector2d(2, 2));
        Assert.assertEquals(animalA.getOrientation(), MapDirection.SOUTH);

        Assert.assertEquals(animalB.getPosition(), new Vector2d(3, 4));
        Assert.assertEquals(animalB.getOrientation(), MapDirection.NORTH);
    }

    @Test
    public void map3Test() {
        String[] directions3 = new String[]{"f", "b", "r", "l", "f", "f", "r", "r", "f", "f", "f", "f", "f", "f", "f", "f"};
        MoveDirection[] parsedDirections3 = OptionsParser.parse(directions3);
        map.run(parsedDirections3);
        Assert.assertEquals(animalA.getPosition(), new Vector2d(2, -1));
        Assert.assertEquals(animalA.getOrientation(), MapDirection.SOUTH);

        Assert.assertEquals(animalB.getPosition(), new Vector2d(3, 7));
        Assert.assertEquals(animalB.getOrientation(), MapDirection.NORTH);
    }

    @Test
    public void map4Test() {
        String[] directions4 = new String[]{"f", "b", "r", "l", "f", "f", "r", "r", "f", "f", "f", "f", "f", "f", "f", "f",
                "r", "l", "r", "l", "b", "b", "b", "b"};
        MoveDirection[] parsedDirections4 = OptionsParser.parse(directions4);
        map.run(parsedDirections4);
        Assert.assertEquals(animalA.getPosition(), new Vector2d(2, -3));
        Assert.assertEquals(animalA.getOrientation(), MapDirection.NORTH);

        Assert.assertEquals(animalB.getPosition(), new Vector2d(3, 9));
        Assert.assertEquals(animalB.getOrientation(), MapDirection.SOUTH);

        Assert.assertSame(map.objectAt(new Vector2d(2, -3)), animalA);
        Assert.assertSame(map.objectAt(new Vector2d(3, 9)), animalB);
    }

    @Test
    public void map5Test() {
        Assert.assertTrue(map.place(new Animal(map, new Vector2d(1, 3))));
    }

    @Test
    public void map6Test() {
        Assert.assertFalse(map.place(new Animal(map, animalA.getPosition())));
    }
}
