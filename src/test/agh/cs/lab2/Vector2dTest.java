package agh.cs.lab2;

import org.junit.Assert;
import org.junit.Test;

public class Vector2dTest {

    @Test
    public void equalsTest() {
        Assert.assertEquals(new Vector2d(2, 3), new Vector2d(2, 3));
        Assert.assertEquals(new Vector2d(0, 0), new Vector2d(0, 0));
        Assert.assertNotEquals(new Vector2d(3, 3), new Vector2d(-1, 3));
        Assert.assertNotEquals(new Vector2d(5, 7), new Vector2d(5, 9));
        Assert.assertNotEquals(new Vector2d(0, 0), new Vector2d(-12, 1));
        Assert.assertNotEquals(new Vector2d(2, 3), "xD");
    }

    @Test
    public void toStringTest() {
        Assert.assertEquals(new Vector2d(5, 7).toString(), "(5,7)");
        Assert.assertEquals(new Vector2d(-3, 4).toString(), "(-3,4)");
        Assert.assertEquals(new Vector2d(0, 0).toString(), "(0,0)");
        Assert.assertEquals(new Vector2d(-0, 0).toString(), "(0,0)");
    }

    @Test
    public void precedesTest() {
        Assert.assertTrue(new Vector2d(2, 3).precedes(new Vector2d(2,3)));
        Assert.assertTrue(new Vector2d(2, 3).precedes(new Vector2d(5,3)));
        Assert.assertTrue(new Vector2d(2, 3).precedes(new Vector2d(2,5)));
        Assert.assertTrue(new Vector2d(2, 3).precedes(new Vector2d(6,4)));
        Assert.assertFalse(new Vector2d(4, -2).precedes(new Vector2d(4,-4)));
        Assert.assertFalse(new Vector2d(4, -2).precedes(new Vector2d(-1,-4)));
        Assert.assertFalse(new Vector2d(4, -2).precedes(new Vector2d(0,0)));
        Assert.assertFalse(new Vector2d(4, -2).precedes(new Vector2d(2,-3)));
    }

    @Test
    public void followsTest() {
        Assert.assertTrue(new Vector2d(7, 3).follows(new Vector2d(7, 3)));
        Assert.assertTrue(new Vector2d(7, 3).follows(new Vector2d(-2, 3)));
        Assert.assertTrue(new Vector2d(7, 3).follows(new Vector2d(7, 2)));
        Assert.assertTrue(new Vector2d(7, 3).follows(new Vector2d(-2, -2)));
        Assert.assertFalse(new Vector2d(2, 1).follows(new Vector2d(3, 1)));
        Assert.assertFalse(new Vector2d(2, 1).follows(new Vector2d(2, 4)));
        Assert.assertFalse(new Vector2d(2, 1).follows(new Vector2d(3, 3)));
        Assert.assertFalse(new Vector2d(0, 0).follows(new Vector2d(3, 1)));
    }

    @Test
    public void upperRightTest() {
        Assert.assertEquals(new Vector2d(3, 3).upperRight(new Vector2d(5, 4)), new Vector2d(5, 4));
        Assert.assertEquals(new Vector2d(1, 2).upperRight(new Vector2d(3, 2)), new Vector2d(3, 2));
        Assert.assertEquals(new Vector2d(3, -1).upperRight(new Vector2d(0, -2)), new Vector2d(3, -1));
    }

    @Test
    public void lowerLeftTest() {
        Assert.assertEquals(new Vector2d(5, 2).lowerLeft(new Vector2d(2, -3)), new Vector2d(2, -3));
        Assert.assertEquals(new Vector2d(1, 2).lowerLeft(new Vector2d(2, 4)), new Vector2d(1, 2));
        Assert.assertEquals(new Vector2d(2, -3).lowerLeft(new Vector2d(3, -5)), new Vector2d(2, -5));
    }

    @Test
    public void addTest() {
        Assert.assertEquals(new Vector2d(2, 3).add(new Vector2d(-2, 4)), new Vector2d(0, 7));
        Assert.assertEquals(new Vector2d(0, 0).add(new Vector2d(0, 0)), new Vector2d(0, 0));
        Assert.assertEquals(new Vector2d(-2, 1).add(new Vector2d(-0, 0)), new Vector2d(-2, 1));
        Assert.assertEquals(new Vector2d(8, -2).add(new Vector2d(-9, 1)), new Vector2d(-1, -1));
    }

    @Test
    public void subtractTest() {
        Assert.assertEquals(new Vector2d(2, 3).subtract(new Vector2d(-2, 4)), new Vector2d(4, -1));
        Assert.assertEquals(new Vector2d(0, 0).subtract(new Vector2d(0, 0)), new Vector2d(0, 0));
        Assert.assertEquals(new Vector2d(-2, 1).subtract(new Vector2d(-0, 0)), new Vector2d(-2, 1));
        Assert.assertEquals(new Vector2d(8, -2).subtract(new Vector2d(-9, 1)), new Vector2d(17, -3));
    }

    @Test
    public void oppositeTest() {
        Assert.assertEquals(new Vector2d(2, 3).opposite(), new Vector2d(-2, -3));
        Assert.assertEquals(new Vector2d(0, 0).opposite(), new Vector2d(0, 0));
        Assert.assertEquals(new Vector2d(-3, 8).opposite(), new Vector2d(3, -8));
        Assert.assertEquals(new Vector2d(-1, -7).opposite(), new Vector2d(1, 7));
    }
}
