package agh.cs.lab3;

public class Animal {
    private MapDirection orientation = MapDirection.NORTH;
    private Vector2d position = new Vector2d(2, 2);

    public String toString() {
        return String.format("Pozycja: (%d,%d), orientacja: %s", this.position.x, this.position.y, orientation.toString());
    }

    public void move(MoveDirection direction) {
        switch (direction) {
            case LEFT:
                this.orientation = this.orientation.previous();
                break;
            case RIGHT:
                this.orientation = this.orientation.next();
                break;
            case FORWARD:
                this.position = this.position.add(this.orientation.toUnitVector());
                break;
            case BACKWARD:
                this.position = this.position.subtract(this.orientation.toUnitVector());
                break;
        }
        this.position = this.position.upperRight(new Vector2d(0, 0));
        this.position = this.position.lowerLeft(new Vector2d(4, 4));
    }

    public MapDirection getOrientation() {
        return orientation;
    }

    public Vector2d getPosition() {
        return position;
    }

}
