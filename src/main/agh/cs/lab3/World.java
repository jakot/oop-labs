package agh.cs.lab3;

public class World {
    public static void main(String[] args) {
        Animal anim = new Animal();
        System.out.println(anim.toString());
        MoveDirection[] directions = OptionsParser.parse(args);

        for (MoveDirection dir : directions) {
            anim.move(dir);
            System.out.println(anim.toString());
        }
    }
}
