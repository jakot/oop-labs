package agh.cs.lab1;

public enum Direction {
    FORWARD, BACKWARD, LEFT, RIGHT;

    static public Direction fromString(String str) {
        switch (str) {
            case "f":
                return FORWARD;
            case "b":
                return BACKWARD;
            case "l":
                return LEFT;
            case "r":
                return RIGHT;
        }
        return FORWARD;
    }
}
