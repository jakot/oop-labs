package agh.cs.lab1;

import static java.lang.System.out;

public class CarSystem {

    private static void run(Direction[] args) {
        for (Direction d : args) {
            switch (d) {
                case FORWARD:
                    out.println("Moving forward"); break;
                case BACKWARD:
                    out.println("Moving backward"); break;
                case LEFT:
                    out.println("Moving left"); break;
                case RIGHT:
                    out.println("Moving right"); break;
            }
        }
        out.print("\b\b");
    }

    public static void main(String[] args) {
        out.println("Start");
        Direction[] converted = new Direction[args.length];
        for (int i = 0; i <converted.length; i++)
            converted[i] = Direction.fromString(args[i]);
        run(converted);
        out.println("Finish");
    }
}
