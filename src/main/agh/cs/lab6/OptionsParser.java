package agh.cs.lab6;

import java.util.Arrays;
import java.util.Objects;

public class OptionsParser {

    static private MoveDirection stringToMoveDirection(String s) throws IllegalArgumentException {
        switch (s) {
            case "f": case "forward": return MoveDirection.FORWARD;
            case "b": case "backward": return MoveDirection.BACKWARD;
            case "l": case "left": return MoveDirection.LEFT;
            case "r": case "right": return MoveDirection.RIGHT;
        }
        throw new IllegalArgumentException(s + " is not legal move specification.");
    }

    static public MoveDirection[] parse(String[] args) {
            return Arrays.stream(args).map(OptionsParser::stringToMoveDirection).toArray(MoveDirection[]::new);
    }
}
