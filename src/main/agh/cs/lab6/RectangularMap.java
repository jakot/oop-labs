package agh.cs.lab6;

public class RectangularMap extends AbstractWorldMap {
    private final int width, height;

    public RectangularMap(int width, int height) {
        super();
        if (width < 0 || height < 0)
            throw new IllegalArgumentException("Width and height must be non-negative.");
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        MapVisualizer visualizer = new MapVisualizer(this);
        return visualizer.draw(new Vector2d(0, 0), new Vector2d(width, height));
    }

    @Override
    public boolean canMoveTo(Vector2d position) {
        return super.canMoveTo(position) && position.follows(new Vector2d(0, 0)) &&
                position.precedes(new Vector2d(width, height));
    }
}
