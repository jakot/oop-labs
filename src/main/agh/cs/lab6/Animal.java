package agh.cs.lab6;

public class Animal implements IMapElement {
    private MapDirection orientation = MapDirection.NORTH;
    private Vector2d position = new Vector2d(2, 2);
    private IWorldMap map;

    public Animal(IWorldMap map) {
        this.map = map;
    }
    public Animal(IWorldMap map, Vector2d initialPosition) {
        this(map);
        this.position = initialPosition;
    }

    public String toString() {
        switch (this.orientation) {
            case EAST: return ">";
            case NORTH: return "^";
            case WEST: return "<";
            case SOUTH: return "v";
        }
        throw new IllegalStateException("Wrong orientation on map.");
    }

    public void move(MoveDirection direction) {
        Vector2d newPosition;
        switch (direction) {
            case LEFT:
                this.orientation = this.orientation.previous();
                return;
            case RIGHT:
                this.orientation = this.orientation.next();
                return;
            case FORWARD:
                newPosition = this.position.add(this.orientation.toUnitVector());
                if (this.map.canMoveTo(newPosition))
                    this.position = newPosition;
                return;
            case BACKWARD:
                newPosition = this.position.subtract(this.orientation.toUnitVector());
                if (this.map.canMoveTo(newPosition))
                    this.position = newPosition;
                return;
            default:
                throw new IllegalStateException(direction + " is wrong direction argument.");
        }
    }

    public MapDirection getOrientation() {
        return this.orientation;
    }
    public Vector2d getPosition() {
        return this.position;
    }
}
