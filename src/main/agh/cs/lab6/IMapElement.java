package agh.cs.lab6;

public interface IMapElement {
    String toString();

    Vector2d getPosition();
}
