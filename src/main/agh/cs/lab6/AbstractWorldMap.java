package agh.cs.lab6;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

abstract class AbstractWorldMap implements IWorldMap {
    protected List<Animal> animals = new ArrayList<>();
    protected HashMap<Vector2d, Animal> animalHashMap = new HashMap<>();

    @Override
    public boolean isOccupied(Vector2d position) {
        return animalHashMap.containsKey(position);
    }

    @Override
    public boolean canMoveTo(Vector2d position) {
        return !isOccupied(position);
    }

    @Override
    public void place(Animal animal) throws IllegalArgumentException {
        if (canMoveTo(animal.getPosition())) {
            animals.add(animal);
            animalHashMap.put(animal.getPosition(), animal);
        }
        else
            throw new IllegalArgumentException(animal.getPosition() + " - place already occupied.");
    }

    @Override
    public void run(MoveDirection[] directions) {
        for (int i = 0; i < directions.length; i++) {
            Animal animal = animals.get(i % animals.size());
            Vector2d oldPosition = animal.getPosition();
            animal.move(directions[i]);
            if (!oldPosition.equals(animal.getPosition())) {
                animalHashMap.remove(oldPosition);
                animalHashMap.put(animal.getPosition(), animal);
            }
        }
    }

    @Override
    public Object objectAt(Vector2d position) {
        return animalHashMap.getOrDefault(position, null);
    }
}
