package agh.cs.lab7;

public interface IMapElement {
    String toString();

    Vector2d getPosition();
}
