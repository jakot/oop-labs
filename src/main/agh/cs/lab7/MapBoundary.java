package agh.cs.lab7;

import java.util.List;
import java.util.TreeMap;

public class MapBoundary implements IPositionChangeObserver {

    private Vector2d upperRight = new Vector2d(0,0);
    private Vector2d lowerLeft = new Vector2d(0,0);
    private TreeMap<Vector2d, IMapElement> treeByX = new TreeMap<>((a, b) -> {
        if(a.x > b.x) return 1;
        else if(a.x < b.x) return -1;
        else return Integer.compare(a.y, b.y);
    });
    private TreeMap<Vector2d, IMapElement> treeByY = new TreeMap<>((a, b) -> {
        if(a.y > b.y) return 1;
        else if(a.y < b.y) return -1;
        else return Integer.compare(a.x, b.x);
    });

    public MapBoundary() { }
    public MapBoundary(List<IMapElement> elements) {
        for (IMapElement element: elements) {
            addElement(element);
        }
    }

    public Vector2d getUpperRight() {
        return upperRight;
    }
    public Vector2d getLowerLeft() {
        return lowerLeft;
    }

    public void addElement(IMapElement element) {
        treeByX.put(element.getPosition(), element);
        treeByY.put(element.getPosition(), element);
        resizeMap();
    }

    @Override
    public void positionChanged(Vector2d oldPosition, Vector2d newPosition) {
        treeByX.put(newPosition, treeByX.remove(oldPosition));
        treeByY.put(newPosition, treeByY.remove(oldPosition));
        resizeMap();
    }

    private void resizeMap() {
        lowerLeft = new Vector2d(treeByX.firstKey().x, treeByY.firstKey().y);
        upperRight = new Vector2d(treeByX.lastKey().x, treeByY.lastKey().y);
    }
}