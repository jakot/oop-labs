package agh.cs.lab7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

abstract class AbstractWorldMap implements IWorldMap, IPositionChangeObserver {
    protected List<Animal> animals = new ArrayList<>();
    protected HashMap<Vector2d, Animal> animalHashMap = new HashMap<>();

    @Override
    public boolean isOccupied(Vector2d position) {
        return animalHashMap.containsKey(position);
    }

    @Override
    public boolean canMoveTo(Vector2d position) {
        return !isOccupied(position);
    }

    @Override
    public void place(Animal animal) throws IllegalArgumentException {
        if (canMoveTo(animal.getPosition())) {
            animals.add(animal);
            animalHashMap.put(animal.getPosition(), animal);
            animal.addObserver(this);
        }
        else
            throw new IllegalArgumentException(animal.getPosition() + " - place already occupied.");
    }

    @Override
    public void positionChanged(Vector2d oldPosition, Vector2d newPosition) {
        Animal animal = animalHashMap.get(oldPosition);
        animalHashMap.remove(oldPosition);
        animalHashMap.put(newPosition, animal);
    }

    @Override
    public void run(MoveDirection[] directions) {
        for (int i = 0; i < directions.length; i++) {
            animals.get(i % animals.size()).move(directions[i]);
        }
    }

    @Override
    public Object objectAt(Vector2d position) {
        return animalHashMap.getOrDefault(position, null);
    }
}
