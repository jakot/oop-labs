package agh.cs.lab7;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class GrassField extends AbstractWorldMap {
    private ArrayList<Grass> grasses;
    private HashMap<Vector2d, Grass> grassHashMap = new HashMap<>();
    private MapBoundary bounds = new MapBoundary();

    public GrassField(ArrayList<Grass> grasses) {
        super();
        this.grasses = grasses;
        for (Grass grass : grasses)
            grassHashMap.put(grass.getPosition(), grass);
    }
    public GrassField(int n) {
        super();
        this.grasses = new ArrayList<>();
        this.grassHashMap = new HashMap<>();
        Random generator = new Random();

        int x, y;
        Vector2d v;
        for (int i = 0; i < n; i++) {
            do {
                x = generator.nextInt((int) Math.sqrt(n*10));
                y = generator.nextInt((int) Math.sqrt(n*10));
                v = new Vector2d(x, y);
            } while (isOccupied(v));
            Grass grass = new Grass(v);
            grasses.add(grass);
            grassHashMap.put(grass.getPosition(), grass);
        }
    }

    @Override
    public String toString() {
        MapVisualizer visualizer = new MapVisualizer(this);

        Vector2d lowerLeft = bounds.getLowerLeft();
        Vector2d upperRight = bounds.getUpperRight();
        return visualizer.draw(lowerLeft, upperRight);
    }

    @Override
    public boolean isOccupied(Vector2d position) {
        return super.isOccupied(position) || grassHashMap.containsKey(position);
    }

    @Override
    public void place(Animal animal) throws IllegalArgumentException {
        super.place(animal);
        bounds.addElement(animal);
    }
    @Override
    public void positionChanged(Vector2d oldPosition, Vector2d newPosition) {
        super.positionChanged(oldPosition, newPosition);
        bounds.positionChanged(oldPosition, newPosition);
    }

    @Override
    public boolean canMoveTo(Vector2d position) {
        Object object = objectAt(position);
        return object == null || object instanceof Grass;
    }
    @Override
    public Object objectAt(Vector2d position) {
        Object object = super.objectAt(position);
        if (object != null)
            return object;
        return grassHashMap.getOrDefault(position, null);
    }
}
