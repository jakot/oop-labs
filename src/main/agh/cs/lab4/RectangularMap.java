package agh.cs.lab4;

import java.util.ArrayList;

public class RectangularMap implements IWorldMap {
    private final int width, height;
    private ArrayList<Animal> animals;

    public RectangularMap(int width, int height) {
        this.width = width;
        this.height = height;
        animals = new ArrayList<>();
    }

    @Override
    public String toString() {
        MapVisualizer visualizer = new MapVisualizer(this);
        return visualizer.draw(new Vector2d(0, 0), new Vector2d(width, height));
    }

    @Override
    public boolean isOccupied(Vector2d position) {
        for (Animal animal : this.animals)
            if (animal.getPosition().equals(position))
                return true;
        return false;
    }
    @Override
    public boolean canMoveTo(Vector2d position) {
        return !this.isOccupied(position) && position.follows(new Vector2d(0, 0)) &&
                position.precedes(new Vector2d(width, height));
    }

    @Override
    public boolean place(Animal animal) {
        if (canMoveTo(animal.getPosition())) {
            animals.add(animal);
            return true;
        }
        return false;
    }

    @Override
    public void run(MoveDirection[] directions) {
        for (int i = 0; i < directions.length; i++)
            animals.get(i % animals.size()).move(directions[i]);
    }

    @Override
    public Object objectAt(Vector2d position) {
        for (Animal animal : animals)
            if (animal.getPosition().equals(position))
                return animal;
        return null;
    }
}
