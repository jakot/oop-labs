package agh.cs.lab5;

import java.util.ArrayList;
import java.util.Random;

public class GrassField extends AbstractWorldMap {
    private ArrayList<Grass> grasses;

    public GrassField(ArrayList<Grass> grasses) {
        super();
        this.grasses = grasses;
    }

    public GrassField(int n) {
        super();
        this.grasses = new ArrayList<>();
        Random generator = new Random();

        int x, y;
        Vector2d v;
        for (int i = 0; i < n; i++) {
            do {
                x = generator.nextInt((int) Math.sqrt(n*10));
                y = generator.nextInt((int) Math.sqrt(n*10));
                v = new Vector2d(x, y);
            } while (isOccupied(v));
            grasses.add(new Grass(v));
        }
    }

    @Override
    public String toString() {
        MapVisualizer visualizer = new MapVisualizer(this);

        Vector2d lowerLeft = grasses.get(0).getPosition();
        Vector2d upperRight = grasses.get(0).getPosition();
        for (Animal animal : animals) {
            lowerLeft = lowerLeft.lowerLeft(animal.getPosition());
            upperRight = upperRight.upperRight(animal.getPosition());
        }
        for (Grass grass : grasses) {
            lowerLeft = lowerLeft.lowerLeft(grass.getPosition());
            upperRight = upperRight.upperRight(grass.getPosition());
        }
        return visualizer.draw(lowerLeft, upperRight);
    }

    @Override
    public boolean isOccupied(Vector2d position) {
        boolean occupied = super.isOccupied(position);
        if (occupied)
            return true;
        for (Grass grass : this.grasses)
            if (grass.getPosition().equals(position))
                return true;
        return false;
    }

    @Override
    public boolean canMoveTo(Vector2d position) {
        Object object = objectAt(position);
        return object == null || object instanceof Grass;
    }
    @Override
    public Object objectAt(Vector2d position) {
        Object object = super.objectAt(position);
        if (object != null)
            return object;
        for (Grass grass : grasses)
            if (grass.getPosition().equals(position))
                return grass;
        return null;
    }
}
