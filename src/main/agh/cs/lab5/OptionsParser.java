package agh.cs.lab5;

import java.util.Arrays;
import java.util.Objects;

public class OptionsParser {

    static private MoveDirection stringToMoveDirection(String s) {
        switch (s) {
            case "f": case "forward": return MoveDirection.FORWARD;
            case "b": case "backward": return MoveDirection.BACKWARD;
            case "l": case "left": return MoveDirection.LEFT;
            case "r": case "right": return MoveDirection.RIGHT;
        }
        return null;
    }

    static public MoveDirection[] parse(String[] args) {
            return Arrays.stream(args).map(OptionsParser::stringToMoveDirection).
                    filter(Objects::nonNull).toArray(MoveDirection[]::new);
    }
}
