package agh.cs.lab5;

import java.util.ArrayList;
import java.util.List;

abstract class AbstractWorldMap implements IWorldMap {
    protected List<Animal> animals = new ArrayList<>();

    @Override
    public boolean isOccupied(Vector2d position) {
        for (Animal animal : this.animals)
            if (animal.getPosition().equals(position))
                return true;
        return false;
    }

    @Override
    public boolean canMoveTo(Vector2d position) {
        return !isOccupied(position);
    }

    @Override
    public boolean place(Animal animal) {
        if (canMoveTo(animal.getPosition())) {
            animals.add(animal);
            return true;
        }
        return false;
    }

    @Override
    public void run(MoveDirection[] directions) {
        for (int i = 0; i < directions.length; i++)
            animals.get(i % animals.size()).move(directions[i]);
    }

    @Override
    public Object objectAt(Vector2d position) {
        for (Animal animal : animals)
            if (animal.getPosition().equals(position))
                return animal;
        return null;
    }
}
