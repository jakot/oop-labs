package agh.cs.lab5;

public interface IMapElement {
    String toString();

    Vector2d getPosition();
}
