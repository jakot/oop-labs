package agh.cs.lab5;

import java.util.ArrayList;

public class World {
    public static void main(String[] args) {
        MoveDirection[] directions = OptionsParser.parse(args);

        ArrayList<Grass> grasses = new ArrayList<>();
        grasses.add(new Grass(new Vector2d(-4, -4)));
        grasses.add(new Grass(new Vector2d(7, 7)));
        grasses.add(new Grass(new Vector2d(3, 6)));
        grasses.add(new Grass(new Vector2d(2, 0)));

        IWorldMap map = new GrassField(grasses);
        map.place(new Animal(map));
        map.place(new Animal(map,new Vector2d(3,4)));

        map.run(directions);
        System.out.println(map);
    }
}
